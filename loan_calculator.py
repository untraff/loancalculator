import re
import sys
from dataclasses import dataclass
from decimal import Decimal

from loguru import logger

from validators import IntegerValidator, DecimalValidator


@dataclass
class LoanProduct:
    """amount: int  # Loan amount
    interest: Decimal  # Interest rate per year
    term: int  # Loan term in years
    downpayment: int = 0  # Initial payment
    """

    amount: int
    interest: Decimal
    term: int
    downpayment: int = 0

    VALIDATORS = {
        "amount": IntegerValidator,
        "interest": DecimalValidator,
        "downpayment": IntegerValidator,
        "term": IntegerValidator,
    }

    @property
    def main_debt(self):
        """debt excluding downpayment"""
        return self.amount - self.downpayment

    @property
    def m_interest(self):
        """share of interest rate per month"""
        return self.interest / 100 / 12

    @property
    def m_term(self):
        """Interest rate per month"""
        return self.term * 12

    @property
    def monthly_payment(self):
        """monthly payment"""
        return self.main_debt * (
            self.m_interest
            + self.m_interest / ((1 + self.m_interest) ** self.m_term - 1)
        )

    @property
    def total_accrued_interest(self):
        """total accrued interest"""
        return self.monthly_payment * self.m_term - self.main_debt

    @property
    def total_payout(self):
        """total payments"""
        return self.monthly_payment * self.m_term

    @classmethod
    def from_list(cls, cmd_args: list):
        data = {
            re.match("[aA-zZ]*", cmd_args[k]).group(0): cmd_args[k + 1]
            for k in range(0, len(cmd_args) - 1, 2)
        }
        pay = {}
        for k, v in data.items():
            validator_class = cls.VALIDATORS.get(k)
            validator = validator_class()
            validated_value = validator.get_valid_value(v)
            pay[k] = validated_value
        payload = {
            k: cls.VALIDATORS.get(k)().get_valid_value(v) for k, v in data.items()
        }
        return cls(**payload)


if __name__ == "__main__" and len(sys.argv) > 6:
    credit_product = LoanProduct.from_list(sys.argv[1:])
    logger.info(
        f"Your monthly payment: {credit_product.monthly_payment.quantize(Decimal('1.00'))}"
    )
    logger.info(
        f"Total amount of accrued interest: {credit_product.total_accrued_interest.quantize(Decimal('1.00'))}"
    )
    logger.info(
        f"Total payments: {credit_product.total_payout.quantize(Decimal('1.00'))}"
    )
