from abc import ABC, abstractmethod
from decimal import Decimal
import re


class Validator(ABC):
    re_pattern = ""

    @abstractmethod
    def get_valid_value(self, value):
        raise NotImplementedError()


class BaseValidator(Validator):
    re_pattern = ".*"

    def get_valid_value(self, value):
        return re.match(self.re_pattern, value).group(0)


class IntegerValidator(BaseValidator):
    def get_valid_value(self, value):
        value = super(IntegerValidator, self).get_valid_value(value)
        value = re.match("([\d]*)", value).group(0)
        return int(value)


class DecimalValidator(BaseValidator):

    def get_valid_value(self, value):
        value = super(DecimalValidator, self).get_valid_value(value)
        symbols = re.match("([\d]*)([\.\, ]*)([\d]*)", value).groups()
        if len(symbols) == 3:
            value = f"{symbols[0]}.{symbols[2]}"
        else:
            value = symbols[0]
        return Decimal(value)
