import unittest
from decimal import Decimal

from loan_calculator import LoanProduct


class TestLoanProduct(unittest.TestCase):
    def setUp(self) -> None:

        self.loan_product = LoanProduct(
            amount=100000, interest=Decimal("5.5"), term=30, downpayment=20000
        )

    @staticmethod
    def custom_round(value: Decimal):
        return value.quantize(Decimal("1.00"))

    def test_create_obj_from_dict(self):
        input_string = "amount: 100000 interest: 5.5% term: 30m downpayment: 20000"
        loan_product_from_dict = LoanProduct.from_list(input_string.split())
        params = ["amount", "interest", "term", "downpayment"]
        [
            self.assertEqual(
                getattr(self.loan_product, param),
                getattr(loan_product_from_dict, param),
            )
            for param in params
        ]

    def test_credit_calculator(self):
        self.assertEqual(
            self.custom_round(self.loan_product.monthly_payment),
            self.custom_round(Decimal(454.2312)),
        )
        self.assertEqual(
            self.custom_round(self.loan_product.total_accrued_interest),
            self.custom_round(Decimal(83523.2323)),
        )
        self.assertEqual(
            self.custom_round(self.loan_product.total_payout),
            self.custom_round(Decimal(163523.2323)),
        )
